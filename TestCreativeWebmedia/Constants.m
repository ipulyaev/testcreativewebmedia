//
//  Constants.m
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/21/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import "Constants.h"

NSString *const StubsURL = @"https://yalantis.com/blog/";

NSTimeInterval const TimeoutInterval = 10.0;

@implementation Constants

@end
