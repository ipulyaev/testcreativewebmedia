//
//  BlogPostsViewController.m
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/20/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import "BlogPostsViewController.h"
#import "Post.h"
#import "Loader.h"
#import "PostCollectionViewCell.h"
#import "FullPostViewController.h"

static NSString *const PostCellIdentifire = @"PostCellIdentifire";
static NSString *const PostCellNibFileName = @"PostCell";

static NSString *const MainStoryboardName = @"Main";
static NSString *const IdentifireOfFullPostViewController = @"FullPostViewController";

static CGFloat const HeightOfPostCell = 350;
static CGFloat const DistanceBetweenPosts = 40;
static CGFloat const CellWidthRatio = 0.48;
static CGFloat const BriefSummaryLabelNumberOfLines = 4;

@interface BlogPostsViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, FullPostDelegate, LoaderDelegate>

@property (nonatomic, strong) IBOutlet UICollectionView *postCollection;
@property (nonatomic, strong) NSArray *blogPosts;
@property (nonatomic, strong) Post *currentPost;

@property  (nonatomic, strong) Loader *loader;

@end

@implementation BlogPostsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Yalantis"];
    
    UICollectionViewFlowLayout *collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
    [self.postCollection setCollectionViewLayout:collectionViewLayout];
    self.postCollection.dataSource = self;
    self.postCollection.delegate = self;

    UINib *cellNib = [UINib nibWithNibName:PostCellNibFileName bundle:nil];
    [self.postCollection registerNib:cellNib forCellWithReuseIdentifier:PostCellIdentifire];
    
    self.blogPosts = [self loadPosts];
}

- (NSArray *)loadPosts {
    if (!self.loader) {
        self.loader = [[Loader alloc] init];
        self.loader.delegate = self;
    }
    return [self.loader getPosts];
}

- (void)showFullPost:(Post *)post {
    self.currentPost = post;
    
    UIStoryboard *mainSB = [UIStoryboard storyboardWithName:MainStoryboardName bundle:nil];
    FullPostViewController *fullPostVC = (FullPostViewController *)[mainSB instantiateViewControllerWithIdentifier:IdentifireOfFullPostViewController];
    fullPostVC.delegate = self;

    [self.navigationController pushViewController:fullPostVC animated:YES];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [self.postCollection.collectionViewLayout invalidateLayout];
}

#pragma mark - LoaderDelegate methods 

- (void)loaderOnDidLoadPosts:(NSArray *)posts {
    self.blogPosts = posts;
    [self.postCollection reloadData];
}

#pragma mark - FullPostDelegate methods

- (NSString *)fullPostDataSource {
    return self.currentPost.originalURLString;
}

- (NSString *)fullPostTitle {
    return self.currentPost.title;
}

#pragma mark - UICollectionViewDataSource methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.blogPosts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PostCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:PostCellIdentifire forIndexPath:indexPath];
    
    Post *post = [self.blogPosts objectAtIndex:indexPath.row];
    [cell.image setImage:[UIImage imageNamed:post.imageURL]];
    [cell.titleLabel setText:post.title];

    cell.briefSummaryLabel.numberOfLines = BriefSummaryLabelNumberOfLines;
    cell.briefSummaryLabel.adjustsFontSizeToFitWidth = YES;
    [cell.briefSummaryLabel setText:post.briefSummary];
    
    [cell.autorLabel setText:post.autor];
    [cell.numberOfViewsLabel setText:[NSString stringWithFormat:@"%lu",(unsigned long)post.numberOfViews]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Post *post = [self.blogPosts objectAtIndex:indexPath.row];
    [self showFullPost:post];
}

#pragma mark - UICollectionViewDelegateFlowLayout methods

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return DistanceBetweenPosts;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize size = CGSizeMake(self.postCollection.frame.size.width, HeightOfPostCell);

    if (self.view.frame.size.width > self.view.frame.size.height) {
        size.width *= CellWidthRatio;
    }
    return size;
}

- (UIEdgeInsets)collectionView: (UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 0, 10, 0);
}

@end

