//
//  Loader.m
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/20/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import "Loader.h"
#import <Foundation/NSJSONSerialization.h>
#import "Post.h"
#import "Constants.h"

@interface Loader() <NSURLConnectionDataDelegate>

@property (nonatomic, strong) NSURLConnection *connectionPosts;
@property (nonatomic, strong) NSMutableData *receivedPostsData;

@end

@implementation Loader

- (NSArray *)getPosts {
    NSURL *url = [NSURL URLWithString:StubsURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TimeoutInterval];
    
    request.HTTPMethod = @"GET";

    self.receivedPostsData = [[NSMutableData alloc] init];

    self.connectionPosts = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (!self.connectionPosts) {
        NSLog(@"loadDescriptionFile connection error!");
    }
    
    return nil;
}

#pragma mark - NSURLConnectionDataDelegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"response from %@", connection.currentRequest.URL.lastPathComponent);
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if ([connection isEqual:self.connectionPosts]) {
        [self.receivedPostsData appendData:data];
    }
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Finish loading %@", connection.currentRequest.URL.lastPathComponent);
    if ([connection isEqual:self.connectionPosts]) {
        NSError *error = nil;
        NSArray *arrayWithPostDict = [NSJSONSerialization JSONObjectWithData:self.receivedPostsData options:kNilOptions error:&error];
        if (error != nil) {
            NSLog(@"Error parsing description file!");
        }
        NSMutableArray *posts = [[NSMutableArray alloc] init];
        for (NSDictionary *postDict in arrayWithPostDict) {
            Post *post = [[Post alloc] initWithDictionary:postDict];
            [posts addObject:post];
        }
        
        [self.delegate loaderOnDidLoadPosts:[NSArray arrayWithArray:posts]];
    }
}


@end
