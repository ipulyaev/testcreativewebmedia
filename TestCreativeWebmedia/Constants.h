//
//  Constants.h
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/21/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const StubsURL;

extern NSTimeInterval const TimeoutInterval;

@interface Constants : NSObject

@end
