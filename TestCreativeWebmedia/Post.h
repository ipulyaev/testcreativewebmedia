//
//  Post.h
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/20/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Post : NSObject

@property (nonatomic, strong) NSString *imageURL;  //  in test it is imageName
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *briefSummary;
@property (nonatomic, strong) NSString *autor;
@property (nonatomic, assign) NSUInteger numberOfViews;

@property (nonatomic, strong) NSString *originalURLString;

- (instancetype)initWithDictionary:(NSDictionary*)dictionary;

@end
