//
//  FullPostViewController.h
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/20/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FullPostDelegate <NSObject>

@required

- (NSString *)fullPostDataSource;
- (NSString *)fullPostTitle;

@end

@interface FullPostViewController : UIViewController

@property (nonatomic, weak) id <FullPostDelegate> delegate;

@end
