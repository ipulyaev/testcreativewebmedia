//
//  Loader.h
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/20/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LoaderDelegate <NSObject>

@required

- (void)loaderOnDidLoadPosts:(NSArray *)posts;

@end

@interface Loader : NSObject

@property (nonatomic, weak) id <LoaderDelegate> delegate;
- (NSArray *)getPosts;

@end
