//
//  FullPostViewController.m
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/20/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import "FullPostViewController.h"
#import "Constants.h"

@interface FullPostViewController ()

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSURL *url;

@end

@implementation FullPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *urlString = [self.delegate fullPostDataSource];
    [self setTitle:[self.delegate fullPostTitle]];
        
    self.url = [NSURL URLWithString:urlString];
    
    self.webView.userInteractionEnabled = YES;
    self.webView.opaque = NO;
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.scrollView.scrollEnabled = YES;

    NSURLRequest* request = [NSURLRequest requestWithURL:self.url
                                             cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                         timeoutInterval:TimeoutInterval];
    [self.webView loadRequest:request];
}

@end
