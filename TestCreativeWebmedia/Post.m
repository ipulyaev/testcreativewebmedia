//
//  Post.m
//  TestCreativeWebmedia
//
//  Created by Igor Puliaiev on 2/20/16.
//  Copyright © 2016 Igor Puliaiev. All rights reserved.
//

#import "Post.h"

@implementation Post

- (instancetype)initWithDictionary:(NSDictionary*)dictionary {
    if (self = [super init]) {
        self.imageURL = dictionary[@"imageURL"];
        self.title = dictionary[@"title"];
        self.briefSummary = dictionary[@"briefSummary"];
        self.autor = dictionary[@"autor"];
        self.numberOfViews = [dictionary[@"numberOfViews"] integerValue];
        
        self.originalURLString = dictionary[@"originalURL"];
    }
    return self;
}

@end
